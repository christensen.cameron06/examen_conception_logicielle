# Examen_conception_logicielle
Ce TP noté repose sur l'utilisation de l'api documentée ici:
https://deckofcardsapi.com/. Le scénario à mettre en oeuvre est le suivant : "En utilisant certaines fonctions présentées dans l'énoncé, mettre en place un scénario executable qui initialise un deck, tire 10 cartes et compte le nombre de cartes tirées de chaque couleur."
 
# Deux parties dans ce TP:

Partie client: /Client

Partie serveur: /Serveur

# Lancement rapide: 

1) Pour lancer le serveur

A la racine du projet entrez dans un terminal :
```python
pip3 install -r requirements.txt

cd Serveur

uvicorn main:app --reload
```
2) Pour le client

A la racine du projet entrez dans un autre terminal :
```python
cd Client
```
Pour lancer l'application entrez dans le terminal :
```python
python3 main.py
```
# Les dépendances 

Les dépendances nécessaires sont listées dans les fichiers requirements.txt

# Appli Pytest &  tests unitaires :
Les tests unitaires permettent la vérifications des briques élémentaires du projet.
Pour lancer les tests : 

```python
pip3 install -r requirements.txt
pytest
```

# Intégration continue
Pour ouvrir une pipeline il faut aller dans CI/CD ==> editor et creer un .gitlab-ci.yml. Cette démarche permet d'avoir un environnement de travail éphemère pour mener par exemple des tests en continue. 

# Adresse http
Lien adresse http: 
http://127.0.0.1:8000
