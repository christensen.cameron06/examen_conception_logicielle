# SERVEUR

Le serveur permet d'utiliser l'API DeckOfCards à l'adresse locale http://localhost:8000

l'url "~/creer-un-deck/" permet de créer un deck et de récupérer son identifiant
l'url "~/cartes/{nombre_cartes}" permet de tirer des cartes dans le deck dont l'identifiant (deck_id) est fourni dans le corps de la requête http au format json.
# Lancement rapide de l'application
Les dépendances nécessaires sont présentes dans le fichier requirements.txt
installer les dépendances:
--------------------------
```python
pip3 install -r requirements.txt 
```

Lancer l'application:
--------------------
```python
uvicorn main:app --reload
```

accéder à l'application:
---------------------
```python
python3 main.py
```
