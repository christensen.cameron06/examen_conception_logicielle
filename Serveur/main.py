#Importation
from fastapi import FastAPI
from fastapi.responses import JSONResponse
from pydantic import BaseModel
import requests
import uvicorn

app = FastAPI()

#Définition d'un item
class Item(BaseModel):
    deck_id: str

#Création d'un deck
@app.get("/creer-un-deck/")
async def creation_deck():
    requete = requests.get("https://deckofcardsapi.com/api/deck/new/\
                           shuffle/?deck_count=1")
    requete1 = requete.json()
    id_deck = requete1["deck_id"]
    return(id_deck)

#API
@app.get("/")
def readRoot():
    return {"https://deckofcardsapi.com/"}




#Tirer ou piocher des cartes
@app.post("/cartes/{nb_cartes}")
def exposer_cartes(nb_cartes: int, item: Item):
    requete = requests.get("https://deckofcardsapi.com/api/deck/" +
                           item.deck_id + "/draw/?count=" +
                           str(nb_cartes))
    requete1 = requete.json()
    return(requete1)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)













