# Importation 
import json
import requests

# Ouvrir un deck 
def ouverture_du_deck():
    requete = requests.get("http://localhost:8000/creer-un-deck/")
    nouveau_deck_id = requete.json()
    return(nouveau_deck_id)

# Tirer des cartes
def tirer_n_cartes(nb_cartes: int, deck_id: str):
    requete = requests.post("http://localhost:8000/cartes/" +
                            str(nb_cartes), json={'deck_id': deck_id},
                            headers={'Content-type': 'application/json',
                                     'Accept': 'text/plain'})
    return(requete.json())

###Calculer, pour une liste de cartes donnée en entrée 
#le nombre de cartes de chaque couleur sous forme d'un dictionnaire et Compter les cartes
def calcul_nb_cartes(fichier_json: dict):
    mon_dictionnaire = {"H": 0, "S": 0, "D": 0, "C": 0}
    for carte in fichier_json["cards"]:
        couleur = carte["code"][1]
        if couleur in mon_dictionnaire.keys():
            mon_dictionnaire[couleur] = mon_dictionnaire[couleur] + 1
    return(mon_dictionnaire)

#Lancement
if __name__ == "__main__":
    r = ouverture_du_deck()
    print(r)
    mon_deck_id = r
    r2 = tirer_n_cartes(10, mon_deck_id)
    print(r2)
    nb_cartes_tirees = calcul_nb_cartes(r2)
    print(nb_cartes_tirees)
