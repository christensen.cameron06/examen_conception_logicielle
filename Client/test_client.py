import pytest
import json
from main import calcul_nb_cartes

# Tests basiques
def load_params_from_json(json_path):
   with open(json_path) as f:
        return json.load(f)

def test_2_egal_2():
  assert 2==2

#Test de la fonction du calcul du nombre de cartes
def test_calcul_nombres_cartes():
    with open('examen_conception_logicielle/Client/fichierTest.json') as json_data:
        data_dict = json.load(json_data)
    assert calcul_nb_cartes(data_dict) == {"H": 1, "S": 2, "D": 0, "C": 0}


if __name__ == "__main__":
    test_calcul_nombres_cartes()
