 # Partie Client
La Partie Client permet de faire des requêtes sur l'API DeckOfCards via le serveur mis en place dans la partie Serveur. Le programme principal main.py réalise: 

La mise en place d'un jeu de 52 cartes via l'API DeckOfCards et retourne l'identifiant deck de cartes.
Le tirage de 10 cartes dans le deck et retourne la liste des cartes tirées dans un dictionnaire au format json.
Le calcul du nombre de cartes par couleur parmi les cartes tirées.


 Environnement virtuel 
-------------------
```
venv/scripts/activate
```
Attention à ne pas versionner l'environnement virtuel. 

 Pour installer les dépendances:
 -------------------------

 ```python
pip3 install -r requirements.txt
```
 Pour Lancer l'application:
---------------------

```python
python3 main.py
```
